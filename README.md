
### The boiler plate for this project was copied from 
[node-express-realworld-example-app](https://github.com/gothinkster/node-express-realworld-example-app)

### Code added by me can be found under routes and models (questionnaire(s) and submissions(s))

### Time spent on MVP: Approximately one evening (6-8 hours)

## Endpoints to try out
- POST - new questionnaire - http://localhost:3000/api/questionnaires 
    - `{"questionnaire": {
            "title":"title",
            "questions":[
                {
                    "heading":"heading question 1",
                    "tag": "question1",
                    "required": true,
                    "answerAlternatives": ["true", "false"]
                },
                {
                    "heading":"heading question 2",
                    "tag": "question2",
                    "required": true,
                    "dependsOnQuestionWithTag": "question1",
                    "dependsOnAnswers": ["true"]
                }
            ]
        }}`
- GET - all questionnaires - http://localhost:3000/api/questionnaires
- GET - one questionnaires - http://localhost:3000/api/questionnaires/:publicId
- POST - new answer submission to one questionnaire - http://localhost:3000/api/submissions/:questionnaireid
    - `{
        "submission": {
            "questionnaireId": "70adfd44-a64b-4e62-86bb-0edfaefa285d",
            "answers":[
            {
                "questionId":"5b51ae60cfaa56fe1c47522c",
                "answer": "Answer to question 1"
            },
            {
                "questionId":"5b51ae60cfaa56fe1c47522d",
                "answer": "Answer to question 2"
            }
            ]
        }
     }`
- GET - all answer submissions for a questionnaire - http://localhost:3000/api/submissions/:questionnaireid
  

## Todo's
- Unit tests for domian logic, integration tests for flow through endpoints and mongoDB
- Proper naming
- Validate answer submission to questionnaire requirements
- Hook questionnaire logic up with auth
- Extend auth with user types and different access levels
- Decouple input domain logic from db domain logic. Validate input objects separately
- Create PUT to manage updates/revisions on questionnaires (same publicid, different questionnaires (so questionnaires can be immutable))
- Track questions across multiple questionnaires with same public id so that data can be bundled when the questions are identical 

# Getting started

To get the Node server running locally:

- Clone this repo
- Make sure you've installed Node, npm and yarn globally
- `yarn install` to install all required dependencies
- Make sure Docker is installed locally
- `yarn mongo:start` to start the local database
- `yarn dev` to start the local server

# Code Overview

## Dependencies

- [expressjs](https://github.com/expressjs/express) - The server for handling and routing HTTP requests
- [express-jwt](https://github.com/auth0/express-jwt) - Middleware for validating JWTs for authentication
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication
- [mongoose](https://github.com/Automattic/mongoose) - For modeling and mapping MongoDB data to javascript 
- [mongoose-unique-validator](https://github.com/blakehaswell/mongoose-unique-validator) - For handling unique validation errors in Mongoose. Mongoose only handles validation at the document level, so a unique index across a collection will throw an exception at the driver level. The `mongoose-unique-validator` plugin helps us by formatting the error like a normal mongoose `ValidationError`.
- [passport](https://github.com/jaredhanson/passport) - For handling user authentication
- [slug](https://github.com/dodo/node-slug) - For encoding titles into a URL-friendly format

## Application Structure

- `app.js` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration for passport as well as a central location for configuration/environment variables.
- `routes/` - This folder contains the route definitions for our API.
- `models/` - This folder contains the schema definitions for our Mongoose models.

## Error Handling

In `routes/api/index.js`, we define a error-handling middleware for handling Mongoose's `ValidationError`. This middleware will respond with a 422 status code and format the response to have [error messages the clients can understand](https://github.com/gothinkster/realworld/blob/master/API.md#errors-and-status-codes)

## Authentication

Requests are authenticated using the `Authorization` header with a valid JWT. We define two express middlewares in `routes/auth.js` that can be used to authenticate requests. The `required` middleware configures the `express-jwt` middleware using our application's secret and will return a 401 status code if the request cannot be authenticated. The payload of the JWT can then be accessed from `req.payload` in the endpoint. The `optional` middleware configures the `express-jwt` in the same way as `required`, but will *not* return a 401 status code if the request cannot be authenticated.

<br />
