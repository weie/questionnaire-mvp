const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const User = mongoose.model('User');
const auth = require('../auth');
const Questionnaire = mongoose.model('Questionnaire');
const Question = mongoose.model('Question');

//TODO, add auth
router.get('/questionnaires', (req, res, next) => 
  Questionnaire
    .find()
    .then((questionnaires) => res.json({questionnaires}))
    .catch(next)
);

//TODO, add auth
router.get('/questionnaires/:publicid', (req, res, next) => 
  Questionnaire
    .findOne({ 'publicid': req.params.publicid})
    .then((questionnaire) => res.json({questionnaire}))
    .catch(next)
);

//TODO, add auth
router.post('/questionnaires', (req, res, next) => {
  var questionnaire = parseQuestionnaire(req.body);

  questionnaire
    .save()
    .then(() => res.json({questionnaire}))
    .catch(next);
});

function parseQuestionnaire(body) {
  const payload = body.questionnaire || {};
  let questionnaire = new Questionnaire(); 

  questionnaire.author = 'author';
  questionnaire.title = payload.title;
  questionnaire.subtitle = payload.subtitle;

  const payloadQuestions = payload.questions || [];
  payloadQuestions
    .forEach((question) => parseQuestion(question, questionnaire));

  return questionnaire;
}

function parseQuestion(payloadQuestion, questionnaire) {
  let question = new Question();
  question.tag = payloadQuestion.tag;
  question.heading = payloadQuestion.heading;
  question.helpText = payloadQuestion.helpText;
  question.answerAlternatives = payloadQuestion.answerAlternatives;
  question.dependsOnQuestionWithTag = payloadQuestion.dependsOnQuestionWithTag
  question.dependsOnAnswers = payloadQuestion.dependsOnAnswers

  questionnaire.questions.push(question);
}

module.exports = router;

