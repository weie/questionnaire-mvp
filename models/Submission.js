let mongoose = require('mongoose');

const AnswerSchema = new mongoose.Schema({
  questionId: { type: mongoose.Schema.Types.ObjectId, ref: 'Question' },
  answer: {type: String, required: [true, "can't be blank"]},
});

const SubmissionSchema = new mongoose.Schema({
  // TODO
  // author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  author: String,  
  questionnaireId: String,
  answers: [AnswerSchema],
}, {timestamps: true});


mongoose.model('Answer', AnswerSchema);
mongoose.model('Submission', SubmissionSchema);
