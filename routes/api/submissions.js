const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const User = mongoose.model('User');
const auth = require('../auth');
const Submission = mongoose.model('Submission');
const Answer = mongoose.model('Answer');
const Questionnaire = mongoose.model('Questionnaire');


//TODO, add auth
router.get('/submissions/:questionnaireid', (req, res, next) => 
    Submission
    .find({ 'questionnaireId': req.params.questionnaireid})
    .then((submissions) => res.json({submissions}))
    .catch(next)
);

//TODO, add auth
router.post('/submissions/:questionnaireid', (req, res, next) => {
  let submission = parseSubmission(req.body); 
  
  submission.questionnaireId = req.params.questionnaireid;
  Questionnaire
    .findOne({ 'publicid': submission.questionnaireId})
    .then((questionnaire) => questionnaire.validateSubmission(submission))
    .then((submission) => 
      submission
      .save()
      .then(() => res.json({submission}))
      .catch(next))
    .catch(next);
});

function parseSubmission(body) {
  const payload = body.submission || {};
  let submission = new Submission(); 

  submission.author = 'author';

  const payloadAnswers = payload.answers || [];
  payloadAnswers
    .forEach((answer) => parseAnswer(answer, submission));

  return submission;
}

function parseAnswer(payloadAnswer, submission) {
  let answer = new Answer();
  answer.questionId = payloadAnswer.questionId;
  answer.answer = payloadAnswer.answer;

  submission.answers.push(answer);
}


module.exports = router;


