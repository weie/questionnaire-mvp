let mongoose = require('mongoose');
const uuid = require('node-uuid');

const QuestionSchema = new mongoose.Schema({
  tag: String,
  heading: {type: String, required: [true, "can't be blank"]},
  helpText: String,
  answerType: {type: String, enum: ['string', 'number', 'multiplechoice'], default: 'string'},
  answerAlternatives: [String],
  dependsOnQuestionWithTag: String,
  dependsOnAnswers: [String],
  required: {type: Boolean, default: false}
});

const QuestionnaireSchema = new mongoose.Schema({
  // TODO
  // author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  author: String,
  publicid: {type: String, default: uuid.v4},
  questions: [QuestionSchema],
  title: {type: String, required: [true, "can't be blank"]},
  subtitle: String
}, {timestamps: true});

QuestionnaireSchema.methods.validateSubmission = (submission) => {
  return new Promise((resolve, reject) => {
    isValidSubmission(submission) 
      ? resolve(submission) 
      : reject("Submission does not match requirements of questionnaire");
  });
};

function isValidSubmission(submission) {
  //TODO, implement validation of submitted answers
  return true
}


mongoose.model('Question', QuestionSchema);
mongoose.model('Questionnaire', QuestionnaireSchema);
